
// copyright text
$(function(){
	var now = new Date();
	var year = now.getFullYear();
	$('#copyright_text').html("COPYRIGHT &copy; 1997 - " + year + " Hitachi Metals, Ltd. ALL RIGHTS RESERVED.");
});

// Odd Even Set
$(function(){
	$('ul').each(function(){
		$(this).find('li:first').addClass('first');
		$(this).find('li:odd').addClass('odd');
		$(this).find('li:even').addClass('even');
		$(this).find('li:last').addClass('last');
	});
	$('dl').each(function(){
		$(this).find('dt:first').addClass('first');
		$(this).find('dt:odd').addClass('odd');
		$(this).find('dt:even').addClass('even');
		$(this).find('dt:last').addClass('last');
		$(this).find('dd:first').addClass('first');
		$(this).find('dd:odd').addClass('odd');
		$(this).find('dd:even').addClass('even');
		$(this).find('dd:last').addClass('last');
	});
	$('table').each(function(){
		$(this).find('tr:first').addClass('first');
		$(this).find('tr:odd').addClass('odd');
		$(this).find('tr:even').addClass('even');
		$(this).find('tr:last').addClass('last');
	});
});

// Inquiry Form Product
function submitProductSpecialtySteel() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="特殊鋼" />');
		$form.append('<input type="hidden" name="UM_category" value="1" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProductRoll() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="ロール" />');
		$form.append('<input type="hidden" name="UM_category" value="2" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProductSoftMagneticMaterials() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="変圧器用軟磁性材料" />');
		$form.append('<input type="hidden" name="UM_category" value="3" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProductNEOMAX() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="マグネット" />');
		$form.append('<input type="hidden" name="UM_category" value="4" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProductInformationSystemComponents() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="軟磁性材料（ソフトフェライト等）" />');
		$form.append('<input type="hidden" name="UM_category" value="5" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProductAutomotiveComponents() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="自動車機器" />');
		$form.append('<input type="hidden" name="UM_category" value="6" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProductPipingComponents() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="配管機器" />');
		$form.append('<input type="hidden" name="UM_category" value="7" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProductOther() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="製品その他" />');
		$form.append('<input type="hidden" name="UM_category" value="0" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProduct09() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product_cable/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="電線・ケーブル(インフラ用)" />');
		$form.append('<input type="hidden" name="UM_category" value="3" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProduct10() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product_cable/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="電線・ケーブル(エレクトロニクス用)" />');
		$form.append('<input type="hidden" name="UM_category" value="3" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProduct11() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product_cable/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="情報ネットワーク製品" />');
		$form.append('<input type="hidden" name="UM_category" value="4" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}
function submitProduct12() {
	$(function(){
		var $form = $('<form action="https://www8.hitachi.co.jp/inquiry/hitachi-metals/product_cable/form.jsp" method="POST"></form>');
		$form.append('<input type="hidden" name="UM_product" value="製品" />');
		$form.append('<input type="hidden" name="UM_category" value="1" />');
		$form.appendTo(document.body);
		$form.submit();
		return false;
	});
}



// pdf count Google Analytics
$(function(){
	$("a[href$='.pdf']").attr("onClick", "_gaq.push(['_trackEvent', 'DownloadPDF', 'click', this.href]);");
});
