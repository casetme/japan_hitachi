function smartRollover(){
	if(document.getElementsByTagName){
		var images = document.getElementsByTagName("img");
		for(var i = 0; i < images.length; i++){
			if(images[i].getAttribute("src").match("_off.")){
				images[i].onmouseover = function(){
					this.setAttribute("src",this.getAttribute("src").replace("_off.","_on."));
				}
				images[i].onmouseout = function(){
					this.setAttribute("src",this.getAttribute("src").replace("_on.","_off."));
				}
			}
		}
	}
}

function advance_load(){
	if(document.getElementsByTagName){
		var images = document.getElementsByTagName("img");
		for(var i = 0; i < images.length; i++){
			if(images[i].getAttribute("src").match("_off.")){
				var imgObj = new Image();
				imgObj.src = images[i].getAttribute("src").replace("_off.","_on.");
			}
		}
	}
}

if(window.addEventListener){
	window.addEventListener("load",smartRollover,false);
	window.addEventListener("load",advance_load,false);
}else if(window.attachEvent){
	window.attachEvent("onload",smartRollover);
	window.attachEvent("onload",advance_load);
}
