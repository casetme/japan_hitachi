(function($){
	$(document).ready(function(){

	//to top
		$("a.pagelink").click(function (){
			var href = $(this).attr("href");
			var p = $(href).offset().top;
			$("html,body").animate({
				scrollTop: p
			},"slow");
			return false;
		});

	//to page
		$(".prevtop").click(function(){
			$("html,body").animate({
				scrollTop: 0
			},"slow");
			return false;
		});

	//menu accordion
		$(".menu_btn").click(function(){
			$("#menu ul").slideToggle("normal");
			return false;
		});

	});
})(jQuery);
